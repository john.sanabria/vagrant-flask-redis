#!/usr/bin/env bash
APP="app.py"
RUNAPP="run.app.sh"
PYTHON_LIBS="redis flask"
sudo apt update
sudo apt install -y python3-pip
sudo pip install ${PYTHON_LIBS}
for A in ${APP} ${RUNAPP}; do 
    sudo cp /vagrant/${A} /home/vagrant/ 
    sudo chmod +x /home/vagrant/${A} 
    sudo chown vagrant:vagrant /home/vagrant/${A}
done
