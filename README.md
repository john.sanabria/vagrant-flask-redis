# Flask, SQL Alchemy

En este directorio se encontrarán los códigos que permiten el __despliegue__ de dos máquinas virtuales donde una de ellas corre una aplicación escrita en Python + Flask y la otra máquina tiene una base de datos de Redis.

La aplicación podrá ser accedida en este enlace <http://192.168.56.2:5000>.

## Las máquinas virtuales

### La aplicación

La aplicación estará hecha en Python y se comportará como una aplicación web a través del microframework [Flask](https://flask.palletsprojects.com/en/3.0.x/).
El código de la aplicación se tomará de la página [Getting started with  docker compose](https://docs.docker.com/compose/gettingstarted/). 

Esta aplicación solo imprime un mensaje indicando el número de veces que ha sido accedida.
El número de  accesos se encuentra en una instancia de la base de datos Redis.
El código de la aplicación se encuentra en `app.py`. 
Para ejecutar la aplicación se usará el script `run.app.sh`.

### La base de datos

La base de datos Redis reside en una máquina virtual donde el motor de base de datos Redis será instalado.

## Archivos donde la infraestructura está codificada

La descripción de las máquinas virtuales se encuentra en el archivo `Vagrantfile`. 

### Aprovisionando la máquina virtual de  la aplicación 

En el script abajo (llamado `script-flask.sh`) se encarga de 

- La instalación del gestor de paquetes de Python `python3-pip`
- Instalación de las librerías `redis, flask` 
- Copia de la aplicación (`app.py`) y el script que permite la ejecución de la aplicación (`run.app.sh`).

```
#!/usr/bin/env bash
APP="app.py"
RUNAPP="run.app.sh"
PYTHON_LIBS="redis flask"
sudo apt update
sudo apt install -y python3-pip
sudo pip install ${PYTHON_LIBS}
for A in ${APP} ${RUNAPP}; do 
    sudo cp /vagrant/${A} /home/vagrant/ 
    sudo chmod +x /home/vagrant/${A} 
    sudo chown vagrant:vagrant /home/vagrant/${A}
done
```

### Aprovisionando la máquina virtual de la base de datos

El script que hace el aprovisionamiento de la base de datos se llama `script-redis.sh`.
Este script se encarga de:

- Instalar el motor de base de datos Redis
- Cambiar el archivo de configuración de la base de datos para que acepte conexiones por el IP `192.168.56.3`.
- Reiniciar el servicio de `redis`.

```
#!/usr/bin/env bash
REDISCONF="redis.conf"
sudo apt update
# Instalacion de redis tomado de 
# https://www.hostinger.com/tutorials/how-to-install-and-setup-redis-on-ubuntu/
sudo apt install -y redis
sudo cp /vagrant/${REDISCONF} /etc/redis
sudo chown redis:redis /etc/redis/${REDISCONF}
sudo chmod 640 /etc/redis/${REDISCONF}
sudo systemctl restart redis
sudo systemctl status redis
```

## Ejecución

Para llevar a cabo el despliegue de estas máquinas virtuales se ejecuta:

```
vagrant up 
```

Una vez las máquinas estén operando se ejecuta el comando:

```
vagrant ssh flask
```

Y una vez dentro de la máquina virtual se ejecuta el comando:

```
. run.app.sh
```

Ahora, usted puede abrir un navegador del equipo __anfitrión (_host_)__ e ir al URL <http://192.168.56.2:5000> y verá en operación su aplicación.

