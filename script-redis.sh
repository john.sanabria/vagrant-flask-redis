#!/usr/bin/env bash
REDISCONF="redis.conf"
sudo apt update
# Instalacion de redis tomado de 
# https://www.hostinger.com/tutorials/how-to-install-and-setup-redis-on-ubuntu/
sudo apt install -y redis
sudo cp /vagrant/${REDISCONF} /etc/redis
sudo chown redis:redis /etc/redis/${REDISCONF}
sudo chmod 640 /etc/redis/${REDISCONF}
sudo systemctl restart redis
sudo systemctl status redis
